/*****************
 * Carl Strickland
 * cmstric
 * 4/14/17
 * This is the header file for the Point class.
 *****************/

#ifndef POINT_H
#define POINT_H

class Point
{
	private:
		double x;
		double y;
	public:
		/*default constructor*/
		Point();
		
		// Overloaded Constructor
		Point(double x, double y);
		
		/*setters*/
		void setX(double);
		void setY(double);
		void setPoint(double, double);

		/*getters*/
		double getX();
		double getY();
};
#endif
