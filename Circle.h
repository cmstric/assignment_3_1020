/*****************
 * Carl Strickland
 * cmstric
 * 4/14/17
 * This is the header file for the Circle class.
 *****************/

#ifndef CIRCLE_H
#define CIRCLE_H
#include "Shape.h"
#include <cmath>
using namespace std;

class Circle: public Shape{
	protected:
		Point center;
		double radius;
	
	public:
	// Default Constructor
	Circle();

	// Overloaded Constructor
	Circle(Point center, double radius);

	// Setters
	void setCenter(Point center);
	void setRadius(double radius);

	// Getters
	Point getCenter();
	double getRadius();

	bool checkCirPoint(Circle cent, Point test);


	// Alter the vector
	void alterCirVec(vector<vector<Shape> > &vec, Circle cir);

	// Set the triangle Points
	void getCirPoints(ifstream &in);
};

#endif
