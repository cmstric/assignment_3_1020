/*****************
 * Carl Strickland
 * cmstric
 * 4/14/17
 * This is the header file for the color class.
 *****************/
#ifndef COLOR_H
#define COLOR_H
	class Color{
  private:
	unsigned char red;
	unsigned char green;
	unsigned char blue;
  public:
	/*Default constructor*/
	Color();
	// Overloaded constructor
	Color(int r, int g, int b);
	
	/*Setters*/
	void setRed(int);
	void setGreen(int);
	void setBlue(int);
	void setColors(int, int, int);
	/*Getters*/
	unsigned char getRed();
	unsigned char getGreen();
	unsigned char getBlue();
};
#endif
