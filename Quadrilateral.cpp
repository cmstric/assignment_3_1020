/*****************
 * Carl Strickland
 * cmstric
 * 4/14/17
 * This is the implementation file for the Quadrilateral class.
 *****************/

#include "Quadrilateral.h"

// Default constructor
Quadrilateral::Quadrilateral(){
	a.setX(0);
	a.setY(0);
	b.setX(0);
	b.setY(0);
	c.setX(0);
	c.setY(0);
	d.setX(0);
	d.setY(0);
}

// Overloaded constructor
Quadrilateral::Quadrilateral(Point a, Point b, Point c, Point D){
	this-> a = a;
	this-> b = b;
	this-> c = c;
	this-> d = d;
}

// Setters
void Quadrilateral::setA(Point a){
	this-> a = a;
}
void Quadrilateral::setB(Point b){
	this-> b = b;
}
void Quadrilateral::setC(Point c){
	this-> c = c;
}
void Quadrilateral::setD(Point d){
	this-> d = d;
}

// Getters
void Quadrilateral::getQuadPoints(ifstream &in){
	double ax, ay;
	double bx, by;
	double cx, cy;
	double dx, dy;

	// Get the doubles from instream
	in >> ax >> ay;
	in >> bx >> by;
	in >> cx >> cy;
	in >> dx >> dy;

	a.setPoint(ax, ay);
	b.setPoint(bx, by);
	c.setPoint(cx, cy);
	d.setPoint(dx, dy);
}

Point Quadrilateral::getA(){
	return a;
}
Point Quadrilateral::getB(){
	return b;
}
Point Quadrilateral::getC(){
	return c;
}
Point Quadrilateral::getD(){
	return d;
}
// check point
	void Quadrilateral::alterQuadVec(vector<vector<Shape> > &vec, \
								Quadrilateral quad){
	Point d, e, f, g;
	Point temp;
	// Set points
	d = quad.getA();
	e = quad.getB();
	f = quad.getC();
	g = quad.getD();
	
	// Set triangles
	Triangle a(d, e, f);
	Triangle b(e, f, g);
	Triangle c(f, g, d);

	// Write to vector the triangle
	a.alterTriVec(vec, a);
	b.alterTriVec(vec, b);
	c.alterTriVec(vec, c);
}
