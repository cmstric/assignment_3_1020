/*****************
 * Carl Strickland
 * cmstric
 * 4/14/17
 * This is the driver file that will implement all of the files included
 * in the classes
 *****************/

// Static function to print the background


#include <fstream>
#include <iostream>
#include <cstdlib>
#include <vector>
#include "Color.h"
#include "Header.h"
#include "Point.h"
#include "Triangle.h"
#include "Quadrilateral.h"
#include "Circle.h"

using namespace std;

static void setBackground(vector<vector<Shape> > &vec);
static void printArray(vector<vector<Shape> > &vec, ofstream &out);


int main(int argc, char *argv[]){
	ifstream input(argv[1]);
	ofstream output(argv[2]);
	int width, height;
   Header headerValues;
	Point test;
	Shape temp;
	string check;
	// Triangle testing
	
	// Point a(300, 65);
	// Point b(200, 50);
	// Point c(22, 100);
	Triangle triPoint;
	Quadrilateral quadPoint;
	Circle circPoint;

	// Check command line arguments
	vector<vector<Shape> > image;

	if(argc != 3)
	{
		cout << "USAGE ERROR:  ./executable outPutFileName";
		exit(EXIT_FAILURE);
  	}
  	if(input.fail())
	{
		cout << argv[1] << " did not open successfully\n";
  	}

  	if(output.fail())
  	{
		cout << argv[2] << " did not open successfully\n";
  	}

   // Print header to output file
  	input >> width >> height;
   headerValues.setWidth(width);
   headerValues.setHeight(height);
   headerValues.printHeader(output);


   // Setting size of the vector
   image.resize(headerValues.getHeight());
   for (int i = 0; i < headerValues.getHeight(); i++) {
   	image[i].resize(headerValues.getWidth());
   }
	// cout << image.size();

	// set vector for background
	setBackground(image);
	input >> check;
	while(!input.eof()){
		if(check == "Triangle"){
			triPoint.getTriPoints(input);	
			// Check the triangle
			triPoint.alterTriVec(image, triPoint);
			input >> check;
		}else if(check == "Quadrilateral"){
			quadPoint.getQuadPoints(input);
			quadPoint.alterQuadVec(image, quadPoint);
			input >> check;
		}else if(check == "Circle"){
			// Circle drawing
			circPoint.getCirPoints(input);
			circPoint.alterCirVec(image, circPoint);
			input >> check;
		}else
			exit(EXIT_FAILURE);
	}

	// printing out the vector
	printArray(image, output);
	


	// Close the file
	input.close();
	return 0;

} // End of main



// Will fill the array with background variables
static void setBackground(vector<vector<Shape> > &vec){
	size_t i, j;
	
	for(i = 0; i < vec.size(); i++){
		for(j = 0; j < vec[i].size(); j++){
			// Loop through and make the background multiple colors
			if(i < (vec.size()/2) && j > (vec[i].size()/2)){
				vec[i][j].setColors(255, 0, 0);
			}else if(i > (vec.size()/2) && j > (vec[i].size()/2)){
				vec[i][j].setColors(0, 0, 255);
			}else{
				vec[i][j].setColors(0, 255, 0);
			}

		}
	}
   return;
}

static void printArray(vector<vector<Shape> > &vec, ofstream &out){
	size_t i, j;
	// For loop to loop and print the rgb pixels to output
	for(i = 0; i < vec.size(); i++){
		for(j = 0; j < vec[i].size(); j++){
			out << vec[i][j].getRed() << vec[i][j].getGreen();
			out << vec[i][j].getBlue();
		}
	}
	return;
}
