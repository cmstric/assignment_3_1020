/*****************
 * Carl Strickland
 * cmstric
 * 4/14/17
 * This is the header file for the header class.
 *****************/

#ifndef HEADER_H
#define HEADER_H
#include <string>
#include <fstream>
using namespace std;


class Header
{
	private:
		int width;
		int height;

	public:
		/*default constructor*/
		Header();

		/*setters*/
		void setWidth(int);
		void setHeight(int);

      // Getters
      int getWidth();
      int getHeight();
		/*print*/
		void printHeader(ofstream&); // Start back here
};

#endif
