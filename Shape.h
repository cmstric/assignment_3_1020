/*****************
 * Carl Strickland
 * cmstric
 * 4/14/17
 * This is the header file for the shape class.
 *****************/

#ifndef SHAPE_H
#define SHAPE_H

#include <string>
#include <vector>
#include <iostream>
//#include "Pixel.h"
#include "Color.h"
#include "Point.h"
#include <stdbool.h>
#include <fstream>

using namespace std;

class Shape: public Color{
	protected:
		string name;
		Color RGB;
	public:
		// Default constructor
		Shape();
		// Setters
		void setName(string shape);
		// Check the name of the
		int checkName(ifstream &in);
		// Print array
		// void setVector(vector<vector<Pixel> > &vec);
		//void setColor(int r, int g, int b);

};

#endif
