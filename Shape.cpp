/*****************
 * Carl Strickland
 * cmstric
 * 4/14/17
 * This is the implementation file for the shape class.
 *****************/

#include "Shape.h"


// constructor
Shape::Shape(){
	name = "";
}

void Shape::setName(string shape){
	name = shape;
}

// Check the Shape it reads in
int Shape::checkName(ifstream &in){
	string test;
	in >> test;
	//cout << test << "\n";
	if(test == "Triangle"){
		return 1;
	}else if(test == "Quadrilateral"){
		return 2;
	}else if(test == "Circle"){
		return 3;
	}else{
		return 0;
	}
}


