/*****************
 * Carl Strickland
 * cmstric
 * 4/14/17
 * This is the header file for the Pixel class.
 *****************/

#ifndef PIXEL_H
#define PIXEL_H
#include "Color.h"
#include "Point.h"
#include <fstream>
using namespace std;


class Pixel
{
	private:
		Color color;
		Point point;
	public:
		/*default constructor*/
		Pixel();

		/*Choose Color*/
		void chooseColor(bool colorChoice); 
		//Chooses which color will be set to the pixel

		/*Set color*/
		void setColor(int r, int g, int b);

		/*Print RGB Values*/
		void printRGB(ofstream& out);	

		/*set point*/
		void setCoord(double xCoord, double yCoord);
};
#endif