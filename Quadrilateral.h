/*****************
 * Carl Strickland
 * cmstric
 * 4/14/17
 * This is the header file for the Quadrilateral class.
 *****************/

#ifndef QUADRILATERAL_H
#define QUADRILATERAL_H

#include <iostream>
#include "Shape.h"
#include "Point.h"
#include "Triangle.h"
#include <stdbool.h>
#include <fstream>
using namespace std;

class Quadrilateral: public Shape{
	protected:
		Point a, b, c, d;
	public:
		// Default constructor
		Quadrilateral();
		// Overloaded constructor
		Quadrilateral(Point a, Point b, Point c, Point D);

		// Setters
		void setA(Point a);
		void setB(Point b);
		void setC(Point c);
		void setD(Point d);

		void getQuadPoints(ifstream &in);
		
		// Getters
		Point getA();
		Point getB();
		Point getC();
		Point getD();

		// Check point
		//void checkQuadPoint(Quadrilateral quad, Point test);

		// Change the vector
		void alterQuadVec(vector<vector<Shape> > &vec, \
								Quadrilateral quad);
		friend bool Triangle::checkTriPoint(Triangle tri, Point test);
};


#endif
