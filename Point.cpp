/*****************
 * Carl Strickland
 * cmstric
 * 4/14/17
 * This is the Implementation file for the Point class.
 *****************/

#include "Point.h"
Point::Point(){
	x = 0;
	y = 0;
}
// Overloaded Constructor
Point::Point(double x, double y){
	this->x = x;
	this->y = y;
}

/*Setters*/
void Point::setX(double xCoord){
	x = xCoord;
}
void Point::setY(double yCoord){
	y = yCoord;
}
void Point::setPoint(double xCoord, double yCoord){
	x = xCoord;
	y = yCoord;
}

/*Getters*/
double Point::getX(){
	return x;
}
double Point::getY(){
	return y;
}
