/*****************
 * Carl Strickland
 * cmstric
 * 4/14/17
 * This is the header file for the Triangle class.
 *****************/

#ifndef TRIANGLE_H
#define TRIANGLE_H
#include "Shape.h"
using namespace std;

class Triangle: public Shape{
	protected:
		Point a, b, c;
	public:
		// Default constructor
		Triangle();
		// Overloaded constructor
		Triangle(Point a, Point b, Point c);

		// Setters
		void setA(Point a);
		void setB(Point b);
		void setC(Point c);
		
		void getTriPoints(ifstream &in);

		//Getters
		Point getA();
		Point getB();
		Point getC();
		// Check point
		bool checkTriPoint(Triangle tri, Point test);

		// Change the vector
		void alterTriVec(vector<vector<Shape> > &vec, Triangle tri);
};
#endif
