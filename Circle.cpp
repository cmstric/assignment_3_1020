/*****************
 * Carl Strickland
 * cmstric
 * 4/14/17
 * This is the implementation file for the Cirlce class.
 *****************/

#include "Circle.h"

// Default Constructor
Circle::Circle(){
	center.setX(0);
	center.setY(0);
	radius = 0;
}

// Overloaded Constructor
Circle::Circle(Point center, double radius){
	this-> center = center;
	this-> radius = radius;
}

// Setters
void Circle::setCenter(Point center){
	this->center = center;
}
void Circle::setRadius(double radius){
	this->radius = radius;
}

// Getters
Point Circle::getCenter(){
	return center;
}
double Circle::getRadius(){
	return radius;
}

bool Circle::checkCirPoint(Circle cent, Point test){
   Point cirPoint = cent.getCenter();
	double cx = cirPoint.getX();
	double cy = cirPoint.getY();
	double dx, dy, R, ax, ay;
	double dx2, dy2;
	// Center points
	ax = test.getX();
	ay = test.getY();

	dx = abs((ax - cx));
	dy = abs((ay - cy));
	R = cent.getRadius();
	dx2 = dx*dx;
	dy2 = dy*dy;

	if(dx+dy <= R){
		return true;
	}else if(dx > R){
		return false;
	}else if(dy > R){
		return false;
	}else if(dx2 + dy2 <= (R*R)){
		return true;
	}else{
		return false;
	}

}

// Alter the vector
void Circle::alterCirVec(vector<vector<Shape> > &vec, Circle cir){
	double i, j;
   //Point a = tri.getA();
   //Point b = tri.getB();
   //Point c = tri.getC();
	Point temp;

	for(i = 0; i < vec.size(); i++){
		temp.setY(i);
		for(j = 0; j < vec[i].size(); j++){
			temp.setX(j);
			if(checkCirPoint(cir, temp)){ 
				vec[i][j].setColors(82, 45, 128);
			}
		}

	}
}

// Set the Circle Points
void Circle::getCirPoints(ifstream &in){
	double ax, ay;
	double rad;

	// Get the doubles from instream
	in >> ax >> ay;
	in >> rad;

	center.setPoint(ax, ay);
	radius = rad;
}
