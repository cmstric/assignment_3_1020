/*****************
 * Carl Strickland
 * cmstric
 * 4/14/17
 * This is the Implementation file for the Pixel class.
 *****************/

#include "Pixel.h"

/*default constructor*/
Pixel::Pixel(){
	setColor(0,0,0);
	setCoord(0, 0);
}

/*Choose Color*/
void Pixel::chooseColor(bool colorChoice){
	if (colorChoice){
		setColor(234, 106, 32);
	}
	else{
		setColor(82, 45, 128);
	}
}

/*Set Color*/
void Pixel::setColor(int r, int g, int b){
	color.setRed(r);
	color.setGreen(g);
	color.setBlue(b);
}

/*Print RGB Values*/
void Pixel::printRGB(ofstream& out){
	out << color.getRed() << color.getGreen() << color.getBlue();
}

/*Set Coordinate*/
void Pixel::setCoord(double xCoord, double yCoord){
	point.setPoint(xCoord, yCoord);
}
