/*****************
 * Carl Strickland
 * cmstric
 * 4/14/17
 * This is the implementation file for the Triangle class.
 *****************/

#include "Triangle.h"

// Default Constructor
Triangle::Triangle(){
	a.setX(0);
	a.setY(0);
	b.setX(0);
	b.setY(0);
	c.setX(0);
	c.setY(0);
}

// Overloaded Constructor
Triangle::Triangle(Point a, Point b, Point c){
	this-> a = a;
	this-> b = b;
	this-> c = c;
}

// Setters
void Triangle::setA(Point a){
	this->a = a;
}
void Triangle::setB(Point b){
	this->b = b;
}
void Triangle::setC(Point c){
	this->c = c;
}

// Getters
Point Triangle::getA(){
	return a;
}
Point Triangle::getB(){
	return b;
}
Point Triangle::getC(){
	return c;
}

bool Triangle::checkTriPoint(Triangle tri, Point test){
   double a = 0;
   double b = 0;
   double c = 0;

   double x = test.getX();
   double y = test.getY();
   
   Point d = tri.getA();
   Point e = tri.getB();
   Point f = tri.getC();

   double x1 = d.getX();
   double x2 = e.getX();
   double x3 = f.getX();
   double y1 = d.getY();
   double y2 = e.getY();
   double y3 = f.getY();

   // Find the a value
   a = ((y2 - y3) * (x - x3) + (x3 - x2) * (y - y3)) /\
       ((y2 - y3) * (x1 - x3) + (x3 - x2) * (y1 - y3));

   // Find the b value
   b  = ((y3 - y1) * (x - x3) + (x1 - x3) * (y - y3)) /\
        ((y2 - y3) * (x1 - x3) + (x3 - x2) * (y1 - y3));

   // Find the c value
   c = 1.0 - a - b;

   if (a <= 1.0 && a >= 0.0 && b <= 1.0 && b >= 0.0 && c <= 1.0 && c >= 0.0) {
      return true;
   }
   else
      return false;
}

// Alter the vector
void Triangle::alterTriVec(vector<vector<Shape> > &vec, Triangle tri){
	double i, j;
   //Point a = tri.getA();
   //Point b = tri.getB();
   //Point c = tri.getC();
	Point temp;

	for(i = 0; i < vec.size(); i++){
		temp.setY(i);
		for(j = 0; j < vec[i].size(); j++){
			temp.setX(j);
			if(checkTriPoint(tri, temp)){ 
				vec[i][j].setColors(234, 106, 32);
			}
		}

	}
}

// Set the triangle Points
void Triangle::getTriPoints(ifstream &in){
	double ax, ay;
	double bx, by;
	double cx, cy;

	// Get the doubles from instream
	in >> ax >> ay;
	in >> bx >> by;
	in >> cx >> cy;

	a.setPoint(ax, ay);
	b.setPoint(bx, by);
	c.setPoint(cx, cy);
}
