SRC = driver.cpp Circle.cpp Color.cpp Point.cpp Quadrilateral.cpp Shape.cpp Triangle.cpp Header.cpp
OBJ = driver.o Circle.o Color.o Point.o Quadrilateral.o Shape.o Triangle.o Header.o
PROG = driver

$(PROG) : $(OBJ)
	g++ $(OBJ) -o $(PROG) -Wall -lm -g

$(OBJ) : $(SRC)


clean:
	rm *.o driver
