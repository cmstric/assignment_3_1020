/*****************
 * Carl Strickland
 * cmstric
 * 4/14/17
 * This is the implementation file for the header class.
 *****************/

#include "Header.h"

/*Default constructor*/
 Header::Header(){
    width = 0;
    height = 0;
}

/*Setters*/

void Header::setWidth(int w){
   width = w;
}

void Header::setHeight(int h) {
   height = h;
}

// Getters
int Header::getWidth(){
   return width;
}

int Header::getHeight(){
   return height;
}
/*Print Header*/
void Header::printHeader(ofstream& out) {
   out << "P6" << "\n" << width << " " <<  height << " " << 255 << "\n";
}
